from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import requests
import csv
import json
import random
import matplotlib.pyplot as plt
from PIL import Image
from io import BytesIO



def imShow(search_term, bot, chat_id):
    assert subscription_key
    search_url = "https://api.cognitive.microsoft.com/bing/v7.0/images/search"

    headers = {"Ocp-Apim-Subscription-Key" : subscription_key}
    params  = {"q": search_term}
    response = requests.get(search_url, headers=headers, params=params)
    response.raise_for_status()
    search_results = response.json()
    #f, axes = plt.subplots(4, 4)
    thumbnail_urls = [img["thumbnailUrl"] for img in search_results["value"][:1]]
    image_data = requests.get(thumbnail_urls[0])
    image_data.raise_for_status()
    print()
    image = Image.open(BytesIO(image_data.content))        
    image.show()
    image.save('out.bmp')
    bot.send_photo(chat_id=chat_id, photo=open('out.bmp', 'rb'))


# токен полученный при регистрации бота
telegram_token = '531320201:AAGqBU6Ke7ivHz4JaYQ3QOPC5v28MNMcrb0'
subscription_key = "ef6dff79e576459883f68ca3c6ab84a8"
#imShow("кремль")

# start вызывается после команды /start
def start(bot, update):
    update.message.reply_text('Добрый день! Я могу определить класс комфортабельности офисного помещения. Какой адрес тебя интересует?')
 
# echo вызывается после любого текстового сообщения
def echo(bot, update):
    chat_id = '373344377'
    #update.message.reply_text(update.message.text)
    req = 'http://geocode-maps.yandex.ru/1.x/?geocode='
    req += update.message.text
    req += '&ll=37.618920,55.756994&results=1&format=json'
    answer = requests.get(req)
    data = json.loads(answer.text, encoding='utf-8')
    #print(data)
    tip = data["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]['kind']
    if (tip == 'house'):
        z = data["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["Address"]["Components"] 
        for i in range(len(z)-1,0,-1): 
            if z[i]["kind"] == "house": 
                house = z[i]["name"] 
            if z[i]["kind"] == "street": 
                street = z[i]["name"] 
            if z[i]["kind"] == "locality": 
                locality = z[i]["name"]
        st = "Город: " + locality + "\nУлица: " + street + "\nДом: " + house
        update.message.reply_text(st)

        pos = data["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"] 
        #result = dict(pos = pos)
        st = 'Географические координаты дома: '
        st += pos
        st += '\nКласс выбранного помещения: '
        classTop = ['A', 'A+', 'B', 'C']
        st += classTop[random.randint(0,3)]
        update.message.reply_text(st)

        zapr = data["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"] 
        imShow(zapr, bot, chat_id)
        print(zapr)
    else:
        st = 'Введите, пожалуйста, адрес вместе с номером дома'
        update.message.reply_text(st)



# создаём основной объект для управления ботом
updater = Updater(telegram_token)
 
# регистрируем процедуру start как обработчик команды start
updater.dispatcher.add_handler(CommandHandler('start', start))
 
# регистрируем процедуру echo как обработчик текстового сообщения
updater.dispatcher.add_handler(MessageHandler(Filters.text, echo))
 
# запускаем бота
updater.start_polling()
updater.idle()